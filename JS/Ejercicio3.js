btnGen.addEventListener("click", function () {
    const edades = [];

    for (let i = 0; i < 100; i++) {
        const edad = genEdad(1, 100); // Puedes ajustar el rango de edades según tus necesidades
        edades.push(edad);
    }

    const edadesSeparadas = edades.join(', ');
    const inputEdades = document.getElementById("txtEdades");

    if (inputEdades) {
        inputEdades.value = edadesSeparadas;
    } else {
        console.error('Elemento input con ID "txtEdades" no encontrado.');
    }

    var bebes = 0;
    var niños = 0;
    var adolescentes = 0;
    var adultos = 0;
    var ancianos = 0;
    var sumaEdades = 0;

    for (var i = 0; i < edades.length; i++) {
        var edad = edades[i];
        
        if (edad >= 1 && edad <= 2) {
            bebes++;
        } else if (edad >= 4 && edad <= 12) {
            niños++;
        } else if (edad >= 13 && edad <= 17) {
            adolescentes++;
        } else if (edad >= 18 && edad <= 60) {
            adultos++;
        } else if (edad >= 61 && edad <= 100) {
            ancianos++;
        }
        
        sumaEdades += edad; // Agrega la edad al acumulador para calcular el promedio
    }

    var promedio = sumaEdades / edades.length;

    document.getElementById("lblBebe").textContent = bebes;
    document.getElementById("lblNiño").textContent = niños;
    document.getElementById("lblAdol").textContent = adolescentes;
    document.getElementById("lblAdul").textContent = adultos;
    document.getElementById("lblAnc").textContent = ancianos;
    document.getElementById("lblProm").textContent = promedio.toFixed(2);

});


btnLim.addEventListener("click", function () {
    document.getElementById('txtEdades').value = "";
});

function genEdad(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
