btnCal.addEventListener("click", function () {
    var cant = parseFloat(document.getElementById('txtCant').value);
    var res = 0;

    if (celFah.checked){
        res = ((9/5) * cant) + 32;
        document.getElementById('txtRes').value = res.toFixed(2);
    }
    else{
        res = (cant - 32) * (5/9);
        document.getElementById('txtRes').value = res.toFixed(2);
    }
});

btnLim.addEventListener("click", function () {
    document.getElementById('txtCant').value = "";
    document.getElementById('txtRes').value = "";
});