btnCal.addEventListener("click", function () {
    const tipo = document.getElementById('selViaje').value;
    var prc = parseFloat(document.getElementById('txtPrc').value);
    var imp = 0;
    var tot = 0;

    if (tipo == 1){
        imp = prc * 0.16;
        document.getElementById('txtSubtot').value = prc;
        document.getElementById('txtImp').value = imp;
        tot = prc + imp;
        document.getElementById('txtTot').value = tot;
    }
    else{
        prc = prc * 1.80;
        imp = prc * 0.16;
        document.getElementById('txtSubtot').value = prc;
        document.getElementById('txtImp').value = imp;
        tot = prc + imp;
        document.getElementById('txtTot').value = tot;
    }
});

btnLim.addEventListener("click", function () {
    document.getElementById('txtBol').value = "";
    document.getElementById('txtCln').value = "";
    document.getElementById('txtDest').value = "";
    document.getElementById('txtPrc').value = "";

    document.getElementById('txtSubtot').value = "";
    document.getElementById('txtImp').value = "";
    document.getElementById('txtTot').value = "";
});